#!/usr/bin/env ruby
# ruby-2.4.1
# Validid 1900 to 2020
# A value that can’t be normalized should be returned as is
def normalize_data(input)
    input[:year] =  input[:year].to_i if is_valid_year?( input[:year].to_i )
    str_make_completed = compleat_make input[:make]
    input[:make] = str_make_completed.capitalize unless str_make_completed == "blah"
    splited_model = split_model_trim input[:model]
    input[:model] = splited_model[0].capitalize if !splited_model[0].nil? && input[:model] != 'foo'
    input[:trim] = normalize_trim input[:trim], splited_model[1]
    input
end
  
# Auxiliar Methods
def is_valid_year? year
  year >= 1900 && year <= 2020
end

def normalize_trim trim, extract_model_trim = nil
  if !extract_model_trim.nil?
    extract_model_trim.upcase
  elsif !trim.nil?
    trim.downcase == 'blank' ? nil : (trim == 'bar' ? 'bar' : trim.upcase)
  else
    trim
  end
end

def split_model_trim model
  array_split_model = model.nil? ? [] : model.split
end

def compleat_make make
  make_colection = ['Ford', 'Chevrolet']
  output = make
  make_colection.each do |mk_str|
    output =  mk_str if mk_str.index(make.capitalize) == 0  && make.length >= 2
  end
  output
end


examples = [
  [{ :year => '2018', :make => 'fo', :model => 'focus', :trim => 'blank' },
   { :year => 2018, :make => 'Ford', :model => 'Focus', :trim => nil }],

  [{ :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' },
   { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }],

  [{ :year => '1999', :make => 'Chev', :model => 'IMPALA', :trim => 'st' },
   { :year => 1999, :make => 'Chevrolet', :model => 'Impala', :trim => 'ST' }],

  [{ :year => '2000', :make => 'ford', :model => 'focus se', :trim => '' },
   { :year => 2000, :make => 'Ford', :model => 'Focus', :trim => 'SE' }]
]

examples.each_with_index do |(input, expected_output), index|
  if (output = normalize_data(input)) == expected_output
    puts "Example #{index + 1} passed!"
  else
    puts "Example #{index + 1} failed,
          Expected: #{expected_output.inspect}
          Got:      #{output.inspect}"
  end
end

